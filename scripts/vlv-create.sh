ldapadd -x -D "cn=Directory Manager" -w Secret123 << EOF
dn: cn=Users cn=users cn=accounts dc=idm dc=lab dc=bos dc=redhat dc=com,cn=userRoot,cn=ldbm database,cn=plugins,cn=config
objectClass: top
objectClass: vlvSearch
cn: Users cn=users cn=accounts dc=idm dc=lab dc=bos dc=redhat dc=com
vlvBase: cn=users,cn=accounts,dc=idm,dc=lab,dc=bos,dc=redhat,dc=com
vlvScope: 1
vlvFilter: (objectclass=*)

dn: cn=by uid cn=users cn=accounts dc=idm dc=lab dc=bos dc=redhat dc=com,cn=Users cn=users cn=accounts dc=idm dc=lab dc=bos dc=redhat dc=com,cn=userRoot,cn=ldbm database,cn=plugins,cn=config
objectClass: top
objectClass: vlvIndex
cn: by ui cn=users cn=accounts dc=idm dc=lab dc=bos dc=redhat dc=com
vlvSort: uid

dn: cn=by givenName cn=users cn=accounts dc=idm dc=lab dc=bos dc=redhat dc=com,cn=Users cn=users cn=accounts dc=idm dc=lab dc=bos dc=redhat dc=com,cn=userRoot,cn=ldbm database,cn=plugins,cn=config
objectClass: top
objectClass: vlvIndex
cn: by givenName cn=users cn=accounts dc=idm dc=lab dc=bos dc=redhat dc=com
vlvSort: givenName

dn: cn=by sn cn=users cn=accounts dc=idm dc=lab dc=bos dc=redhat dc=com,cn=Users cn=users cn=accounts dc=idm dc=lab dc=bos dc=redhat dc=com,cn=userRoot,cn=ldbm database,cn=plugins,cn=config
objectClass: top
objectClass: vlvIndex
cn: by sn cn=users cn=accounts dc=idm dc=lab dc=bos dc=redhat dc=com
vlvSort: sn
EOF

service dirsrv stop IDM-LAB-BOS-REDHAT-COM

/var/lib/dirsrv/scripts-IDM-LAB-BOS-REDHAT-COM/vlvindex -n userRoot -T "by uid cn=users cn=accounts dc=idm dc=lab dc=bos dc=redhat dc=com"
/var/lib/dirsrv/scripts-IDM-LAB-BOS-REDHAT-COM/vlvindex -n userRoot -T "by givenName cn=users cn=accounts dc=idm dc=lab dc=bos dc=redhat dc=com"
/var/lib/dirsrv/scripts-IDM-LAB-BOS-REDHAT-COM/vlvindex -n userRoot -T "by sn cn=users cn=accounts dc=idm dc=lab dc=bos dc=redhat dc=com"

service dirsrv start IDM-LAB-BOS-REDHAT-COM
