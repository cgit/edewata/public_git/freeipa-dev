#!/bin/sh +x

. ./include.sh

json="{
    \"method\": \"sudocmd_del\",
    \"params\": [
        [ \"/usr/bin/less\" ],
        {
            \"all\": true,
            \"rights\": true
        }
    ],
    \"id\": 0
}"\

curl\
  -H "Content-Type:application/json"\
  -H "Accept:applicaton/json"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  $IPA_JSON_URL
