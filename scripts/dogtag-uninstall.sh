#!/bin/sh -x

yum erase -y\
	dogtag-pki-ca-theme\
	dogtag-pki-common-theme\
	pki-ca\
	pki-setup\
	pki-silent
