#!/bin/sh +x

./uninstall-server.sh
./uninstall-rpms.sh

cd ../../freeipa
rm -rf dist/rpms
rm -rf rpmbuild
rm -rf /usr/share/ipa
make rpms
cd -

./install-rpms.sh
./install-dogtag.sh
