#!/bin/sh +x

. ./include.sh

json="{
    \"method\": \"batch\",
    \"params\": [
        [
            {
                \"method\": \"hbac_remove_accesstime\",
                \"params\": [
                    [ \"test\" ],
                    {
                        \"accesstime\": \"periodic daily 0800-1400\"
                    }
                ]
            },
            {
                \"method\": \"hbac_remove_accesstime\",
                \"params\": [
                    [ \"test\" ],
                    {
                        \"accesstime\": \"absolute 201012161032 ~ 201012161033\"
                    }
                ]
            }
        ],
        { }
    ],
    \"id\":0
}"

curl\
  -H "Content-Type:application/json"\
  -H "Accept:applicaton/json"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  $IPA_JSON_URL
