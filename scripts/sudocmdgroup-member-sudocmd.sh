#!/bin/sh +x

. ./include.sh

json="{
    \"method\": \"batch\",
    \"params\": [
        [
            {
                \"method\": \"sudocmd_show\",
                \"params\": [
                    [ \"/usr/bin/more\" ],
                    {
                        \"all\": true,
                        \"rigts\": true
                    }
                ]
            },
            {
                \"method\": \"sudocmd_show\",
                \"params\": [
                    [ \"/usr/bin/less\" ],
                    {
                        \"all\": true,
                        \"rigts\": true
                    }
                ]
            }
        ],
        { }
    ],
    \"id\":0
}"

curl\
  -H "Content-Type:application/json"\
  -H "Accept:applicaton/json"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  $IPA_JSON_URL
