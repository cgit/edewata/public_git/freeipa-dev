#!/bin/sh -x

user=$1

if [ "$user" == "" ]; then
    home=$HOME
else
    home=/home/$user
fi

echo HOME=$home

SRC_DIR=`cd ../.. ; pwd`

FIREFOX_DIR=$home/.mozilla/firefox
PROFILE=`grep Path= $FIREFOX_DIR/profiles.ini | awk -F= '{print $2}'`

CA_INSTANCE_NAME=ca-master
KRA_INSTANCE_NAME=kra-master

################################################################################
# Importing CA certificate
################################################################################

CA_CERT_NAME="caSigningCert cert-$CA_INSTANCE_NAME CA"
CA_CERT_DIR=/var/lib/pki/$CA_INSTANCE_NAME/alias

# export CA cert
certutil -L -d $CA_CERT_DIR -n "$CA_CERT_NAME" -a > $CA_CERT_DIR/ca.pem
AtoB $CA_CERT_DIR/ca.pem $CA_CERT_DIR/ca.crt

# import CA cert
certutil -A -d $FIREFOX_DIR/$PROFILE -n "$CA_CERT_NAME" -i $CA_CERT_DIR/ca.pem -t CT,C,C

################################################################################
# Importing server certificate
################################################################################

SERVER_CERT_NAME="Server-Cert cert-$CA_INSTANCE_NAME"

# export server cert
certutil -L -d $CA_CERT_DIR -n "$SERVER_CERT_NAME" -a > $CA_CERT_DIR/server.pem
AtoB $CA_CERT_DIR/server.pem $CA_CERT_DIR/server.crt

# import server cert
certutil -A -d $FIREFOX_DIR/$PROFILE -n "$SERVER_CERT_NAME" -i $CA_CERT_DIR/server.pem -t CT,C,C

################################################################################
# Importing CA admin certificate
################################################################################

CA_CERT_P12=$CA_CERT_DIR/ca_admin_cert.p12

# import CA admin cert
pk12util -i $CA_CERT_P12 -d $FIREFOX_DIR/$PROFILE -W Secret123
certutil -M -n caadmin -t u,u,u -d $FIREFOX_DIR/$PROFILE

################################################################################
# Importing KRA admin certificate
################################################################################

KRA_CERT_DIR=/var/lib/pki/$KRA_INSTANCE_NAME/alias
KRA_CERT_P12=$KRA_CERT_DIR/kra_admin_cert.p12

# import KRA admin cert
pk12util -i $KRA_CERT_P12 -d $FIREFOX_DIR/$PROFILE -W Secret123
certutil -M -n kraadmin -t u,u,u -d $FIREFOX_DIR/$PROFILE
