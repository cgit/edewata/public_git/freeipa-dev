#!/bin/sh +x

. ./include.sh

json="{
    \"method\": \"hbac_mod\",
    \"params\": [
        [ \"test\" ],
        {
            \"description\": \"\",
            \"accessruletype\": \"deny\",
            \"hostcategory\": \"\",
            \"servicecategory\": \"\",
            \"sourcehostcategory\": \"\",
            \"usercategory\": \"\",
            \"all\": true,
            \"rights\": true
        }
    ],
    \"id\": 0
}"

curl\
  -H "Content-Type:application/json"\
  -H "Accept:applicaton/json"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  $IPA_JSON_URL
