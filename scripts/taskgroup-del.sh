#!/bin/sh +x

curl\
  -H "Content-Type:application/json"\
  -H "Accept:applicaton/json"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d '{"method":"taskgroup_del","params":[["test"],{}],"id":0}'\
  -X POST\
  https://dev.example.com/ipa/json
