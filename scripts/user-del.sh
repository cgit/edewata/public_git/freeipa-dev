#!/bin/sh +x

. ./include.sh

json="{
    \"method\": \"user_del\",
    \"params\": [
        [ \"testuser\" ],
        { }
    ],
    \"id\": 0
}"

curl\
  -H "Content-Type:application/json"\
  -H "Accept:applicaton/json"\
  --delegation always\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  $IPA_JSON_URL
