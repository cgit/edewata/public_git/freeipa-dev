#!/bin/sh +x

. ./include.sh

json="{
    \"method\": \"group_add\",
    \"params\": [
        [ \"testgroup\" ],
        {
            \"description\": \"Test Group\",
            \"user\": \"testuser\"
        }
    ],
    \"id\": 0
}"

curl\
  -H "Content-Type: application/json"\
  -H "Accept: applicaton/json"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  $IPA_JSON_URL
