#!/bin/sh +x

. ./include.sh

json="{
    \"method\": \"host_mod\",
    \"params\": [
        [ \"test.example.com\" ],
        {
            \"all\": true,
            \"rights\": true,
            \"userpassword\": \"test\"
        }
    ],
    \"id\": 0
}"\

curl\
  -H "Content-Type:application/json"\
  -H "Accept:applicaton/json"\
  --negotiate -u :\
  --delegation always\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  $IPA_JSON_URL
