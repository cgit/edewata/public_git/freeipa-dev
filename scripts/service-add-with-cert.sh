#!/bin/sh +x

cert=`cat example.b64`

url="https://dev.example.com/ipa/json"
#url="http://localhost:8888/ipa/json"

json="{
    \"method\": \"service_add\",
    \"params\": [
        [ \"test/dev.example.com@DEV.EXAMPLE.COM\" ],
        { \"usercertificate\": { \"__base64__\": \"$cert\" } }
    ],
    \"id\":0
}"\

curl\
  -H "Content-Type:application/json"\
  -H "Accept:applicaton/json"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  $url
