yum -y install ruby rubygems ruby-devel gcc perl-Locale-Msgfmt tomcat6 java-1.6.0-openjdk-devel python-rhsm
gem update --system
export JAVA_HOME=/usr/lib/jvm/java-1.6.0/
gem install rspec -v 1.3.1
gem install cucumber -v 0.9.0
gem install buildr rest-client 
git clone git://git.fedorahosted.org/git/candlepin.git/
cd candlepin/proxy
buildr clean test=no package
buildconf/scripts/deploy
/sbin/service tomcat6 restart
ln -s /etc/candlepin/certs/keystore /usr/share/tomcat6/conf/keystore
ln -s /etc/candlepin/certs/candlepin-ca.crt /etc/candlepin/certs/candlepin-ca.pem
mkdir /etc/rhsm
hostname=`hostname`
cat > /etc/rhsm/rhsm.conf << EOF
[server]
# Server hostname:
hostname=$hostname

# Server prefix:
prefix=/candlepin

port=8443

proxy_hostname=
proxy_port=

# Set to 1 to disable certificate validation:
insecure = 0

ca_cert_dir = /etc/candlepin/certs
EOF
wget -qO- http://localhost:8080/candlepin/admin/init
echo "Now you just need to load some candlepin data"
echo "and install IPA"
