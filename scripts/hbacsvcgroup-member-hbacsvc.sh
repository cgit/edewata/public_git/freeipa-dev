#!/bin/sh +x

. ./include.sh

json="{
    \"method\": \"batch\",
    \"params\": [
        [
            {
                \"method\": \"hbacsvc_show\",
                \"params\": [
                    [ \"sudo\" ],
                    {
                        \"all\": true,
                        \"rigts\": true
                    }
                ]
            },
            {
                \"method\": \"hbacsvc_show\",
                \"params\": [
                    [ \"sudo-i\" ],
                    {
                        \"all\": true,
                        \"rigts\": true
                    }
                ]
            }
        ],
        { }
    ],
    \"id\":0
}"

curl\
  -H "Content-Type:application/json"\
  -H "Accept:applicaton/json"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  $IPA_JSON_URL
