#!/bin/sh +x

. ./include.sh

json="{
    \"method\": \"host_find\",
    \"params\": [
        [ ],
        {
            \"all\": true,
            \"rights\": true
        }
    ]
}"\

curl\
  -H "Content-Type: application/json"\
  -H "Accept: applicaton/json"\
  --negotiate -u :\
  --delegation always\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  $IPA_JSON_URL
