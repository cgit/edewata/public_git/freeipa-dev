#!/bin/sh +x

curl\
  -H "Content-Type:application/json"\
  -H "Accept:applicaton/json"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d '{"method":"user_add","params":[["test"],{
    "givenname":"Test",
    "sn":"User",
    "telephoneNumber":"123-456-7890"
  }],"id":0}'\
  -X POST\
  https://dev.example.com/ipa/json
