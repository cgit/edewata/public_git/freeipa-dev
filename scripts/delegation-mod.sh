#!/bin/sh +x

. ./include.sh

json="{
    \"method\": \"delegation_mod\",
    \"params\": [
        [ \"test\" ],
        {
            \"attrs\": \"cn,sn\"
        }
    ],
    \"id\": 0
}"

#            \"attrs\": [
#                \"carlicense\",
#                \"cn\"
#            ],
#            \"group\": \"editors\",
#            \"memberof\": \"editors\",
#            \"permissions\": [
#                \"write\"
#            ]
#            \"all\": 0,
#            \"raw\": 0,
#            \"version\": \"2.0\"

curl\
  -H "Content-Type:application/json"\
  -H "Accept:applicaton/json"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  $IPA_JSON_URL
