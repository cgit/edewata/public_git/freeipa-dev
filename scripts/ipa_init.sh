#!/bin/sh +x

. ./include.sh

curl -v\
 -H "Content-Type: application/json"\
 -H "Accept: applicaton/json"\
 --negotiate -u :\
 --cacert /etc/ipa/ca.crt\
 -d @ipa_init_request.json\
 -X POST\
 $IPA_JSON_URL > ipa_init_response.json
