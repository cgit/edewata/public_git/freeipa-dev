#!/bin/sh +x

. ./include.sh

json="{
    \"method\": \"sudocmdgroup_add\",
    \"params\": [
        [ \"group1\" ],
        {
            \"description\": \"Group 1\",
            \"all\": true,
            \"rights\": true
        }
    ],
    \"id\": 0
}"\

curl\
  -H "Content-Type:application/json"\
  -H "Accept:applicaton/json"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  $IPA_JSON_URL
