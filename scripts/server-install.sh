#!/bin/sh -x

FORWARDER=`grep nameserver /etc/resolv.conf | head -1 | awk '{print $2;}'`
DOMAIN=`dnsdomainname`
REALM=${DOMAIN^^}

ipa-server-install\
  -d\
  -U\
  -r $REALM\
  -p Secret123\
  -a Secret123\
  --no-pkinit\
  --setup-dns\
  --forwarder $FORWARDER\
  --no-ntp
