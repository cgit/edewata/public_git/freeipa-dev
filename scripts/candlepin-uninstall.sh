#!/bin/sh

rm -rf /var/lib/candlepin
rm -rf /var/log/candlepin
rm -rf /var/cache/candlepin
rm -rf /var/lib/tomcat6/webapps/candlepin
rm -rf /var/lib/tomcat6/webapps/candlepin.war
rm -rf /etc/candlepin
rm -rf /usr/share/tomcat6/conf/keystore

service tomcat6 restart
