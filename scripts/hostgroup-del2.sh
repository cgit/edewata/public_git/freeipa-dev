#!/bin/sh +x

. ./include.sh

json="{
    \"method\": \"hostgroup_del\",
    \"params\": [
        [ \"staging\" ],
        { }
    ],
    \"id\":0
}"

curl\
  -H "Content-Type:application/json"\
  -H "Accept:applicaton/json"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  https://dev.example.com/ipa/json
