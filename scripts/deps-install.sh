#!/bin/sh -x

cd ../../freeipa

yum erase -y\
	samba-client\
	samba-common

yum install -y\
	rpm-build\
	`grep "^BuildRequires" freeipa.spec.in | awk '{ print $2 }' | grep -v "^/"`

yum install -y\
	`grep "^Requires" freeipa.spec.in | awk '{ print $2 }' | grep -v "^/"`
