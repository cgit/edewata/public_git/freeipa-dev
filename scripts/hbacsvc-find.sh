#!/bin/sh +x

. ./include.sh

json="{
    \"method\": \"hbacsvc_find\",
    \"params\": [
        [ \"\" ],
        { }
    ],
    \"id\":0
}"\

curl\
  -H "Content-Type:application/json"\
  -H "Accept:applicaton/json"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  $IPA_JSON_URL
