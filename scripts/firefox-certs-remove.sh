#!/bin/sh

user=$1

if [ "$user" == "" ]; then
    home=$HOME
else
    home=/home/$user
fi

CA_INSTANCE_NAME=pki-tomcat
FIREFOX_DIR=$home/.mozilla/firefox
PROFILE=$HOSTNAME

cd $FIREFOX_DIR/$PROFILE

certutil -D -n "$HOSTNAME" -d .
certutil -D -n "$HOSTNAME #2" -d .
certutil -D -n "$HOSTNAME #3" -d .
certutil -D -n "Certificate Authority" -d .
