#!/bin/sh +x

. ./include.sh

json="{
    \"method\": \"entitle_register\",
    \"params\": [
        [ \"admin\" ],
        {
            \"all\": true,
            \"password\": \"admin\",
            \"ipaentitlementid\": \"4a8a601a-1cf1-4484-868c-c4cfacfdffed\"
        }
    ],
    \"id\":0
}"

curl\
  -H "Content-Type: application/json"\
  -H "Accept: applicaton/json"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  $IPA_JSON_URL
