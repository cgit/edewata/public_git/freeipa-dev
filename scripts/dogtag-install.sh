#!/bin/sh -x

yum install -y\
	dogtag-pki-ca-theme\
	dogtag-pki-common-theme\
	pki-ca\
	pki-setup\
	pki-silent
