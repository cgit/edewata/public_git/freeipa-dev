#!/bin/sh +x

curl\
  -H "Content-Type:application/json"\
  -H "Accept:applicaton/json"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d '{"method":"service_add_host","params":[["test/dev.example.com@DEV.EXAMPLE.COM"],{"host":"dev.example.com"}],"id":0}'\
  -X POST\
  https://dev.example.com/ipa/json
