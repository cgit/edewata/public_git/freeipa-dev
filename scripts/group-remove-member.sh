#!/bin/sh +x

. ./include.sh

json="{
    \"method\": \"group_remove_member\",
    \"params\": [
        [ \"testgroup\" ],
        {
            \"user\": \"testuser,admin\"
        }
    ]
}"

# \"user\": [ \"testuser\", \"admin\" ]
#    \"id\": 0

curl\
  -H "Content-Type: application/json"\
  -H "Accept: applicaton/json"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  $IPA_JSON_URL
