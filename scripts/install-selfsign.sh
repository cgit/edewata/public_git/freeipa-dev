#!/bin/sh -x

IP_ADDRESS=`ifconfig eth0 | grep "inet " | awk '{print $2}'`
FORWARDER=`grep nameserver /etc/resolv.conf | awk '{print $2;}'`

ipa-server-install\
  -d\
  -U\
  -r REDHAT.COM\
  -n redhat.com\
  -p Secret123\
  -a Secret123\
  --selfsign\
  --setup-dns\
  --forwarder $FORWARDER\
  --no-ntp
