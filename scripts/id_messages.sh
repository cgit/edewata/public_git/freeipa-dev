#!/bin/sh +x

. ./include.sh

json="{
    \"method\": \"i18n_messages\",
    \"params\": [
        [ ],
        { }
    ],
    \"id\": 0
}"\

curl\
  -H "Content-Type: application/json"\
  -H "Accept: applicaton/json"\
  -H "Accept-Language: id"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  $IPA_JSON_URL
