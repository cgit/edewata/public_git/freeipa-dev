#!/bin/sh +x

. ./include.sh

json="{
    \"method\": \"batch\",
    \"params\": [
        [
            {
                \"method\": \"sudocmdgroup_remove_member\",
                \"params\": [
                    [ \"group1\" ],
                    {
                        \"sudocmd\": \"/usr/bin/less\"
                    }
                ]
            },
            {
                \"method\": \"sudocmdgroup_remove_member\",
                \"params\": [
                    [ \"group2\" ],
                    {
                        \"sudocmd\": \"/usr/bin/more\"
                    }
                ]
            }
        ],
        { }
    ],
    \"id\":0
}"

curl\
  -H "Content-Type:application/json"\
  -H "Accept:applicaton/json"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  $IPA_JSON_URL
