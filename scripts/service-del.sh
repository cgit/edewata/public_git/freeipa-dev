#!/bin/sh +x

curl\
  -H "Content-Type:application/json"\
  -H "Accept:applicaton/json"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d '{"method":"service_del","params":[["test/dev.example.com@DEV.EXAMPLE.COM"],{}],"id":0}'\
  -X POST\
  https://dev.example.com/ipa/json
