#!/bin/sh +x

. ./include.sh

json="{
    \"method\": \"delegation_show\",
    \"params\": [
        [ \"test\" ],
        {
            \"all\": true
        }
    ],
    \"id\":0
}"

curl\
  -H "Content-Type:application/json"\
  -H "Accept:applicaton/json"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  $IPA_JSON_URL
