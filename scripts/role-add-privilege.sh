#!/bin/sh +x

. ./include.sh

json="{
    \"method\": \"role_add_privilege\",
    \"params\": [
        [ \"Test Role\" ],
        {
            \"privilege\": \"Write, Add and Delete Members\"
        }
    ]
}"

curl\
  -H "Content-Type: application/json"\
  -H "Accept: applicaton/json"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  $IPA_JSON_URL
