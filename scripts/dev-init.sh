#!/bin/sh -x

cd ../../freeipa

yum-builddep -y freeipa.spec
yum install -y python-sssdconfig bind-pkcs11 bind-dyndb-ldap
yum install -y haveged

systemctl start haveged.service
