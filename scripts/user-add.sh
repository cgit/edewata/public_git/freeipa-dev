#!/bin/sh +x

. ./include.sh

json="{
    \"method\": \"user_add\",
    \"params\": [
        [ \"testuser\" ],
        {
            \"givenname\": \"Test\",
            \"sn\": \"User\",
            \"telephoneNumber\": \"123-456-7890\",
            \"mail\": [
                \"testuser@example.com\",
                \"test@example.com\"
            ]
        }
    ],
    \"id\": 0
}"

curl\
  -H "Content-Type: application/json"\
  -H "Accept: applicaton/json"\
  --delegation always\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  $IPA_JSON_URL
