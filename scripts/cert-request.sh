#!/bin/sh +x

#ipa -vvv cert-request --principal=test/test.example.com `pwd`/example.csr
#exit

. ./include.sh

#csr=`cat example.csr`
csr="-----BEGIN CERTIFICATE REQUEST-----\n\
MIIBhjCB8AIBADBHMR8wHQYDVQQKDBZJRE0uTEFCLkJPUy5SRURIQVQuQ09NMSQw
IgYDVQQDDBt0ZXN0LmlkbS5sYWIuYm9zLnJlZGhhdC5jb20wgZ8wDQYJKoZIhvcN
AQEBBQADgY0AMIGJAoGBANg4V4TpjXqRDWBK+WdRT8s5pCVuxieR84Jn1Q5p6DVF
spcUglgHSzUMBrxuIjyBvpmnMONwC01kdyE4G1eeXQEE86GQXqcPjjhIibPp5QEK
ZwI14XiWCY1PFRJbYCRaXi+BcbBZKXFKsEhzKUiqwgwx1iDvWZR5PK4kuK3xsZB3
AgMBAAGgADANBgkqhkiG9w0BAQUFAAOBgQDK90KmQ/aJKiaVEYR0JHzuu7ux9fVz
uFW42pgYYHdZb3BvkcP7cFeGUpvy0phdAEHEkemqH4WdWUqhMvcHfkKe+kgcmBQO
J35jzXd087r70j3a93V7AvtfwDUCbHiJ12q7OHUuOgiimcEYfdQkwKwAzwBF+g4H
laSFJXQWyOVv5A==
-----END CERTIFICATE REQUEST-----"

json="{
    \"method\": \"cert_request\",
    \"params\": [
        [ \"$csr\" ],
        {
            \"principal\": \"host/test.example.com\"
        }
    ],
    \"id\":0
}"\

#echo $json

curl\
  -H "Content-Type: application/json"\
  -H "Accept: applicaton/json"\
  -H "Referer: https://dev.example.com/ipa/xml"\
  --negotiate\
  --cacert /etc/ipa/ca.crt\
  -u :\
  -d "$json"\
  -X POST\
  $IPA_JSON_URL
