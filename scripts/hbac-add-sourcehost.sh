#!/bin/sh +x

. ./include.sh

json="{
    \"method\": \"hbac_add_sourcehost\",
    \"params\": [
        [ \"test\" ],
        {
            \"host\": \"dev.example.com\",
            \"hostgroup\": \"staging\"
        }
    ],
    \"id\":0
}"\

curl\
  -H "Content-Type:application/json"\
  -H "Accept:applicaton/json"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  $IPA_JSON_URL
