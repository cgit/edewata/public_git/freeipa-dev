#!/bin/sh

REPLICA=$1
IP_ADDRESS=$2

ipa-replica-prepare -p Secret123 --ip-address=$IP_ADDRESS $REPLICA

mkdir -p replica
scp /var/lib/ipa/replica-info-${REPLICA}.gpg replica
