#!/bin/sh +x

. ./include.sh

curl -v\
 -H "Content-Type: application/json"\
 -H "Accept: applicaton/json"\
 --negotiate -u :\
 --cacert /etc/ipa/ca.crt\
 -d @dns-cluster-1.json\
 -X POST\
 $IPA_JSON_URL
