#!/bin/sh +x

# /usr/bin/pkiremove -pki_instance_root=/var/lib -pki_instance_me=pki-ca --force

rpm -e pki-silent pki-native-tools pki-util pki-java-tools pki-ca pki-symkey pki-common pki-setup pki-selinux 

rpm -e dogtag-pki-ca-theme dogtag-pki-common-theme
