#!/bin/sh -x

FIREFOX_DIR=~/.mozilla/firefox
PROFILE=`grep Path= $FIREFOX_DIR/profiles.ini | awk -F= '{print $2}'`

cd $FIREFOX_DIR/$PROFILE

certutil -D -n "EXAMPLE.COM Certificate Authority" -d .
certutil -D -n "$HOSTNAME" -d .
