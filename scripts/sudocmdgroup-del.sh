#!/bin/sh +x

. ./include.sh

json="{
    \"method\": \"sudocmdgroup_del\",
    \"params\": [
        [ \"group1\" ],
        {
            \"all\": true,
            \"rights\": true
        }
    ],
    \"id\": 0
}"\

curl\
  -H "Content-Type:application/json"\
  -H "Accept:applicaton/json"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  $IPA_JSON_URL
