#!/bin/sh +x

. ./include.sh

curl\
  -H "Content-Type: application/json"\
  -H "Accept: applicaton/json"\
  --delegation always\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d @json/hbactest.json\
  -X POST\
  $IPA_JSON_URL
