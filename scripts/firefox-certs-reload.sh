#!/bin/sh -x

user=$1

if [ "$user" == "" ]; then
    home=$HOME
else
    home=/home/$user
fi

echo HOME=$home

./firefox-certs-remove.sh $user
./firefox-certs-import.sh $user
