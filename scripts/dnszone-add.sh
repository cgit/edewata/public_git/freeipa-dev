#!/bin/sh +x

. ./include.sh

#            \"name_from_ip\": \"192.168.1.0/24\",
json="{
    \"method\": \"dnszone_add\",
    \"params\": [
        [ ],
        {
            \"name_from_ip\": \"FED0:0011:2233::/64\",
            \"idnssoamname\": \"ns.example.com.\",
            \"force\": true
        }
    ],
    \"id\": 0
}"

curl\
  -H "Content-Type: application/json"\
  -H "Accept: applicaton/json"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  $IPA_JSON_URL
