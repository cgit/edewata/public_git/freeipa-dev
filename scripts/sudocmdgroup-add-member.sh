#!/bin/sh +x

. ./include.sh

json="{
    \"method\": \"sudocmdgroup_add_member\",
    \"params\": [
        [ \"group1\" ],
        {
            \"sudocmd\": \"/usr/bin/more,/usr/bin/less\"
        }
    ],
    \"id\":0
}"

curl\
  -H "Content-Type:application/json"\
  -H "Accept:applicaton/json"\
  --negotiate -u :\
  --cacert /etc/ipa/ca.crt\
  -d "$json"\
  -X POST\
  $IPA_JSON_URL
