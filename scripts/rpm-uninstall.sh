#!/bin/sh -x

yum erase -y\
	freeipa-admintools\
	freeipa-python\
	freeipa-server\
	freeipa-client\
	freeipa-server-selinux\
	freeipa-debuginfo
