#!/bin/sh -x

user=$1

if [ "$user" == "" ]; then
    home=$HOME
else
    home=/home/$user
fi

FIREFOX_DIR=$home/.mozilla/firefox
PROFILE=$HOSTNAME

certutil -L -d $FIREFOX_DIR/$PROFILE
