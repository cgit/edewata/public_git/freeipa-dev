#!/bin/sh -x

FIREFOX_DIR=~/.mozilla/firefox
PROFILE=`grep Path= $FIREFOX_DIR/profiles.ini | awk -F= '{print $2}'`

cd $FIREFOX_DIR/$PROFILE

certutil -L -d .
