#!/bin/sh +x

. ./include.sh

filename=$1

echo Sending $filename...

curl -v\
 -H "Content-Type: application/json"\
 -H "Accept: applicaton/json"\
 -H "Referer: https://dev.example.com/ipa/xml"\
 --negotiate -u :\
 --delegation always\
 --cacert /etc/ipa/ca.crt\
 -d @$filename\
 -X POST\
 $IPA_JSON_URL
